/*
 * Mouse detection
 *
 * Copyright (C) 1999, Caldera System Inc
 */

#ifndef MDETECT_H
#define MDETECT_H

struct device {
	struct device *	next;
	char		name[64];
	int		fd;
	struct mouse *	driver;
	unsigned long	timeout;	/* When packet should be complete */

	int		flags;
#define DEV_DRIVER_FOUND	0x01
#define DEV_SERIAL_PORT		0x02
	struct termios	tty;
};

struct mouse {
	const char *	name;
	const char *	xfree86_protocol_name;
	int		(*init)(struct device *, struct mouse *);
	int		(*packet)(struct device *,
					struct mouse *,
					unsigned char);
	int		(*timeout)(struct device *);
	const char *	(*report)(struct mouse *);

	struct mouse *	next;
	struct mouse *	alt;
	void *		data;

	unsigned char	buffer[256];
	unsigned int	cur, got;
	unsigned int	total;		/* total byte count */
	unsigned int	good;
	unsigned int	bad;

	unsigned int	flags;
#define FLAG_MS_WEIRDO	0x01		/* could be serial m$ 3/5 proto */

	unsigned int	buttons;
#define BUTTON_RIGHT	0x01
#define BUTTON_MIDDLE	0x02
#define BUTTON_LEFT	0x04
	int		dx, dy, dz;
	int		lx, ly, lz;	/* last dx/dy, for 2nd derivative */
};

#define MIN_GOOD_EVENTS		20	/* Number of good events after which
					 * we accept a driver */
#define SERIAL_IDLE_TIMEOUT	100	/* If we don't see at least one
					 * event after 100ms, we switch
					 * to the next serial device */
#define SERIAL_NOISE_MAX	8	/* Max number of bad chars after
					 * which we switch to the next driver
					 */

extern struct mouse *		get_mouse(const char *);
int			send_event(struct mouse *);
void			serialdev_init(struct device *);

extern int		opt_verbose;
extern int		opt_nopnp;

#if defined(__linux__)
# define DEV_SERIAL_PREFIX		"ttyS"
#elif defined(__FreeBSD_kernel__)
# define DEV_SERIAL_PREFIX		"cuaa"
# define DEV_PSAUX_PREFIX		"psm"
# define DEV_USB_PREFIX			"ums"
# define DEV_BUSMOUSE_PREFIX		"mse"
#endif

#endif /* MDETECT_H */
